#aliases
alias restart_pipewire="systemctl --user restart pipewire pipewire-pulse wireplumber"
alias git_format_cpp="git clang-format --extensions 'cpp,h,hpp,c' --force"
alias lg="lazygit"
alias q="exit"
alias update-all-packages='pacman -Syu && flatpak update'
alias export_qtlog='export QT_LOGGING_RULES="*.debug=true;qt.*=false;quotient.*=false"'
alias srcvenv='source .venv/bin/activate'
alias hx="helix"
0file() { curl -F"file=@$1" {0} ; }
0pb() { curl -F"file=@-;" https://envs.sh ; }
0url() { curl -F"url=$1" https://envs.sh ; }
0short() { curl -F"shorten=$1" https://envs.sh ; }