#!/usr/bin/env bash

# Output IDs from `wpctl status`
# $speakers = speakers
# $headset = headset
headset=$(wpctl status | grep 'EPOS GSA 70 Game' | sed 's/.*\([[:digit:]][[:digit:]]\).*EPOS.*/\1/')
speakers=$(wpctl status | grep 'Navi 21/23 HDMI/DP Audio Controller Digital Stereo' | sed 's/.*\([[:digit:]][[:digit:]]\).*Navi.*/\1/')
output=$(cat /tmp/sound_output)

case $output in
  $headset)
    wpctl set-default $speakers
    echo $speakers > /tmp/sound_output
    ;;
  $speakers)
    wpctl set-default $headset
    echo $headset > /tmp/sound_output
    ;;
  *)
    notify-send -i audio-headphones-symbolic -a "Audio Output" "No default found, check sinks. Setting to $headset." &
    wpctl set-default $headset
    echo $headset > /tmp/sound_output
    ;;
esac
