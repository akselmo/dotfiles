#aliases
alias restart_pipewire="systemctl --user restart pipewire pipewire-pulse wireplumber"
alias git_format_cpp="git clang-format --extensions 'cpp,h,hpp,c' --force"
alias lg="lazygit"
alias q="exit"
alias update-all-packages='sudo dnf update --refresh && flatpak update'
alias update-all-packages_yes='sudo dnf update --refresh -y && flatpak update -y'
alias export_qtlog='export QT_LOGGING_RULES="*.debug=true;qt.*=false;quotient.*=false"'
alias srcvenv='source .venv/bin/activate'
#alias hx="helix"
alias ls="lsd"
alias restart_plasmashell="systemctl --user restart plasma-plasmashell.service"
alias zide="zellij --layout ~/.config/zellij/layouts/helix.kdl"

alias build_plasma="kde-builder workspace breeze kate dolphin konsole spectacle ark discover kdeplasma-addons krdp"
alias build_plasma_with_qt="kde-builder qt6-set workspace kate dolphin konsole spectacle ark discover kdeplasma-addons"
alias krb="kde-builder --no-include-dependencies --no-src --refresh-build"
alias q="exit"
alias kdesrc-build="kde-builder"
alias screen_res_1920="kscreen-doctor output.DP-3.mode.1920x1080@165"
alias screen_res_2560="kscreen-doctor output.DP-3.mode.2560x1080@165"
alias screen_res_3440="kscreen-doctor output.DP-3.mode.3440x1440@165"
