#!/usr/bin/env python3
import os
import time
import colorsys
import configparser
from pathlib import Path
import subprocess
import sys
import hid  # pip install hid


class Accent(object):
    current_accent_color = []
    # mouse_name = "Tunable RGB Gaming Mouse G502"
    sleep_time = 5
    vendor_id = 0x3434
    product_id = 0x0221
    usage_page = 0xFF60
    usage = 0x61

    def __init__(self):
        self._cached_stamp = 0
        self.filename = str(Path.home()) + "/.config/kdeglobals"
        self.config = configparser.ConfigParser()
        self.get_raw_hid_interface()
        while True:
            self.check_for_file_change()
            time.sleep(self.sleep_time)

    def check_for_file_change(self):
        stamp = os.stat(self.filename).st_mtime
        if stamp != self._cached_stamp:
            self._cached_stamp = stamp
            self.read_accent_color()

    def read_accent_color(self):
        with open(self.filename, "r") as f:
            self.config.read(self.filename)
            self.set_accent_color(self.config.get("General", "AccentColor").split(","))

    def set_accent_color(self, new_color):
        if self.current_accent_color != new_color:
            self.current_accent_color = new_color
            self.run_shell_command()

    def run_shell_command(self):
        print("run shell command to change color")
        hsv_val = colorsys.rgb_to_hsv(
            int(self.current_accent_color[0]) / 255,
            int(self.current_accent_color[1]) / 255,
            int(self.current_accent_color[2]) / 255,
        )
        hex_val = "%02x%02x%02x" % (
            int(self.current_accent_color[0]),
            int(self.current_accent_color[1]),
            int(self.current_accent_color[2]),
        )
        hue = int(hsv_val[0] * 255)
        sat = int(hsv_val[1] * 255)
        self.send_raw_packet(
            [
                # 0x07: set value, 0x08 get value
                # https://github.com/Keychron/qmk_firmware/blob/aa73362674028bf6603a7a2df701f4617e09f8cb/quantum/via.h#L81
                # 0x83 rgblight color
                # https://github.com/the-via/reader/blob/edc9b366065d6bd8fa551172f1d5f143d677471d/src/types.v2.ts#L38
                # hue, saturation
                # value seems unused?
                0x07,
                0x83,
                hue,
                sat,
            ]
        )

        print(int(hsv_val[2]))
        # cmd = 'openrgb -d "{0}" -c {1} -m static'.format(self.mouse_name, hex_val)
        # print(cmd)
        # return_code = subprocess.call(cmd, shell=True)
        # print(str(return_code))

    # stolen from https://gist.github.com/fauxpark/03a3efcc7dbdfbfe57791ea267b13c55#file-rawhid-py
    def get_raw_hid_interface(self):
        device_interfaces = hid.enumerate(self.vendor_id, self.product_id)
        raw_hid_interfaces = [
            i
            for i in device_interfaces
            if i["usage_page"] == self.usage_page and i["usage"] == self.usage
        ]

        if len(raw_hid_interfaces) == 0:
            return None

        interface = hid.Device(path=raw_hid_interfaces[0]["path"])

        print("Manufacturer: %s" % interface.manufacturer)
        print("Product: %s" % interface.product)

        return interface

    def send_raw_packet(self, data):
        interface = self.get_raw_hid_interface()

        if interface is None:
            print("No device found")
            sys.exit(1)

        request_data = [0x00] * 33  # First byte is Report ID
        request_data[1 : len(data) + 1] = data
        request_packet = bytes(request_data)

        print("Request:")
        print(request_packet)

        try:
            interface.write(request_packet)

            response_packet = interface.read(32, timeout=1000)

            print("Response:")
            print(response_packet)
        finally:
            interface.close()


if __name__ == "__main__":
    accent = Accent()
