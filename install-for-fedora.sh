#!/usr/bin/env sh
read -p "You may want to do big update first! Press any key to continue installation."
echo "Remove things i dont need"
sudo dnf remove kontact akregator konversation elisa-player dragon kwrite
echo "Update"
sudo dnf update
echo "======Install stuff======"
echo "RPM fusion"
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
echo "Actually working codecs from RPM fusion"
sudo dnf install ffmpeg-libs --allowerasing
echo "Installing basic tools like gcc, git.."
sudo dnf install -y gcc clang clang-tools-extra clazy cmake meson make git gdb lldb dnf-plugins-core kate
sudo dnf copr enable atim/lazygit -y
sudo dnf install -y lazygit
sudo dnf install -y helix
sudo dnf install -y glfw-devel
sudo dnf builddep -y glfw
sudo dnf install -y libubsan libasan
sudo dnf install -y compat-libpthread-nonshared
sudo dnf builddep -y compat-libpthread-nonshared
echo "Wine stuff"
sudo dnf install -y winetricks wine-mono wine wine-dxvk dxvk-native
echo "Gaming"
sudo dnf install -y steam xonotic
sudo dnf copr enable atim/heroic-games-launcher -y
sudo dnf install -y heroic-games-launcher-bin
echo "Chat"
sudo dnf -y install nheko
echo "Other"
sudo dnf install -y qownnotes audacious kasts lmms inkscape gimp audacity blender tiled qpwgraph vlc keepassxc nextcloud-client
sudo dnf -y groupupdate core
echo "========installs done======="

echo "Downloading and moving plasma theme stuff"
sleep 1
echo "Using discover to install themes and colorschemes"
plasma-discover --search revontuli
read -p "Look for zephyr stuff next"
plasma-discover --search zephyr
read -p "Press any key to continue"

echo "Helix colors"
cd $HOME/Repositories/ && git clone https://codeberg.org/akselmo/Revontuli.git
mkdir -p $HOME/.config/helix/themes/
cp $HOME/Repositories/Revontuli/Helix/revontuli.toml $HOME/.config/helix/themes/

echo "Symlink helix config"
cd $HOME/Repositories/dotfiles/
ln -s $HOME/Repositories/dotfiles/helix/config.toml $HOME/.config/helix/config.toml

echo "Symlink lazygit config"
mkdir -p $HOME/.config/lazygit/
cd $HOME/Repositories/dotfiles/
ln -s $HOME/Repositories/dotfiles/lazygit/config.yml $HOME/.config/lazygit/config.yml

echo "Set up kate"
kate
echo "1. Import colorscheme from Revontuli"
echo "2. Set debug plugin and LSP plugin configs to the one in dotfile folder"
read -p "Press any key to continue"

read -p "Setting oh-my-bash after you press enter"
echo "Setting up oh-my-bash"
bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh) --unattended" #i know, i know..

echo "Oh-my-bash theme"
mkdir -p $HOME/.oh-my-bash/themes/aks/
curl https://codeberg.org/akselmo/aks_themes/raw/branch/main/oh-my-bash/aks.theme.sh >> $HOME/.oh-my-bash/themes/aks/aks.theme.sh
chmod +x $HOME/.oh-my-bash/themes/aks/aks.theme.sh

echo "Copy .bash_aliases etc"
cp ./profiles/.* $HOME/
mkdir -p $HOME/.config/plasma-workspace/env/
cp ./plasma/path.sh $HOME/.config/plasma-workspace/env/

echo "Set krunner to meta key"
kwriteconfig5 --file kwinrc --group ModifierOnlyShortcuts --key Meta "org.kde.krunner,/App,,toggleDisplay"
#echo "Set overview to meta key"
#kwriteconfig5 --file kwinrc --group ModifierOnlyShortcuts --key Meta "org.kde.kglobalaccel,/component/kwin,,invokeShortcut,Overview"
qdbus org.kde.KWin /KWin reconfigure

echo "Last bit needs manual work:"
echo "Sync firefox"
echo "Setup nextcloud"
echo "Setup email"
echo "Setup calendar"
echo "Setup password manager"
echo "Add your gpg key with kgpg"
echo "Set zephyr"
echo "Set custom fonts"
echo "Set Freetube and its configs"
echo "Add the audio script to login"
echo "Openmw needs to be get from flathub"
echo "Then, reboot!"
