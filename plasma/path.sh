# Place this in ~/.config/plasma-workspace/env/

# Turn off telemetry crap
export GOTELEMETRY=off
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:" ]]
then
    PATH="$HOME/.local/bin:$PATH"
fi
export PATH

# Java (enable when needed)
#export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))
#export PATH=$PATH:$JAVA_HOME/bin
#export CLASSPATH=.:$JAVA_HOME/jre/lib:$JAVA_HOME/lib:$JAVA_HOME/lib/tools.ja