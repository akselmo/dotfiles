#!/usr/bin/env sh
read -p "Before you do anything, please run pacman -Syu to update your system! Press any key to continue."

echo "Enable fstrim just in case"
systemctl enable --now fstrim.timer

echo "Create user dirs"
xdg-user-dirs-update
mkdir $HOME/Repositories
mkdir $HOME/Nextcloud
read -p "You may wish to change the icons of these folders. Press enter key to continue."

echo " # enabling parallel downloads, color, and check space"
#sed -i 's/#\[multilib\]/[multilib]/' /etc/pacman.conf
#sed -i '/\[multilib\]/!b;n;cInclude = /etc/pacman.d/mirrorlist' /etc/pacman.conf
sudo sed -i 's/#Color/Color/' /etc/pacman.conf
sudo sed -i 's/#CheckSpace/CheckSpace/' /etc/pacman.conf
sudo sed -i 's/#ParallelDownloads = .*/ParallelDownloads = 5/' /etc/pacman.conf
echo " # updating pacman repos and keyring"
sudo pacman -Syy --noconfirm
sudo pacman -Sy --noconfirm --needed archlinux-keyring


echo "======Install stuff======"
echo "Installing basic tools like gcc, git.."
sudo pacman -S gcc clang clazy cmake meson make git helix wl-clipboard base-devel audacious kasts kmail kalendar krita gwenview okular libreoffice-fresh kgpg keepassxc lazygit nextcloud-client kleopatra spectacle kdeconnect cups seahorse gutenprint print-manager nheko gst-plugins-bad gst-plugins-ugly gst-plugins-good gstreamer-vaapi qt-gstreamer noto-fonts-cjk noto-fonts adobe-source-han-sans-jp-fonts kdepim-addons ttf-liberation terminus-font xdg-desktop-portal p7zip
echo "Installing GAMIN"
read -p "Remember to select correct driver when installing steam: AMD: vulkan-radeon | NVIDIA: nvidia-utils | Intel: vulkan-intel"
sudo pacman -S steam winetricks wine wine-mono wine-gecko xonotic
echo "Install papirus icons"
sudo pacman -S papirus-icon-theme
# Gonna just try flatpak version of this first
# echo "=======Install AUR stuff======="
# echo "Build and setup yay"
# cd $HOME/Repositories && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && cd
# If you want octopi for KDE as package manager
# yay -S octopi octopi-notifier-frameworks
# Use discover for flatpaks only!
#echo "======Install FLATPAK stuff====="
#flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#echo "Logseq"
#flatpak install flathub com.logseq.Logseq
#echo "Heroic"
#flatpak install flathub com.heroicgameslauncher.hgl
#echo "Freetube"
#flatpak install flathub io.freetubeapp.FreeTube

echo "Downloading and moving plasma theme stuff"
sleep 1
echo "Using discover to install themes and colorschemes"
plasma-discover --search revontuli
read -p "Press any key to continue"

echo "Helix colors"
cd $HOME/Repositories/ && git clone https://codeberg.org/akselmo/Revontuli.git
mkdir -p $HOME/.config/helix/themes/
cp $HOME/Repositories/Revontuli/Helix/revontuli.toml $HOME/.config/helix/themes/

echo "Symlink helix config"
cd $HOME/Repositories/dotfiles/
ln -s $HOME/Repositories/dotfiles/helix/config.toml $HOME/.config/helix/config.toml

echo "Set up kate"
kate
echo "1. Import colorscheme from Revontuli"
echo "2. Set debug plugin and LSP plugin configs to the one in dotfile folder"
read -p "Press any key to continue"

read -p "Setting oh-my-bash after you press enter"
echo "Setting up oh-my-bash"

bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh) --unattended" #i know, i know..

echo "Oh-my-bash theme"
mkdir -p $HOME/.oh-my-bash/themes/aks/
curl https://codeberg.org/akselmo/aks_themes/raw/branch/main/oh-my-bash/aks.theme.sh >> $HOME/.oh-my-bash/themes/aks/aks.theme.sh
chmod +x $HOME/.oh-my-bash/themes/aks/aks.theme.sh

echo "Copy .bash_aliases etc"
cp ./profiles/.* $HOME/

#echo "Set krunner to meta key"
#kwriteconfig5 --file kwinrc --group ModifierOnlyShortcuts --key Meta "org.kde.krunner,/App,,toggleDisplay"
echo "Set overview to meta key"
kwriteconfig5 --file kwinrc --group ModifierOnlyShortcuts --key Meta "org.kde.kglobalaccel,/component/kwin,,invokeShortcut,Overview"
qdbus org.kde.KWin /KWin reconfigure

echo "Select pinentry for gpg"
echo "pinentry-program /usr/bin/pinentry-qt" >> $HOME/.gnupg/gpg-agent.conf

echo "Enable cups"
systemctl enable --now cups

echo "Setting terminus font for TTY"
echo "FONT=ter-v16n" | sudo tee -a /etc/vconsole.conf

echo "Install kate git"
echo -e "\n" | sudo tee -a /etc/pacman.conf
echo "[home_akselmo_kate-git-arch_Arch]" | sudo tee -a /etc/pacman.conf
echo "Server = https://download.opensuse.org/repositories/home:/akselmo:/kate-git-arch/Arch/$arch" | sudo tee -a /etc/pacman.conf
key=$(curl -fsSL https://download.opensuse.org/repositories/home:akselmo:kate-git-arch/Arch/$(uname -m)/home_akselmo_kate-git-arch_Arch.key)
fingerprint=$(gpg --quiet --with-colons --import-options show-only --import --fingerprint <<< "${key}" | awk -F: '$1 == "fpr" { print $10 }')
sudo pacman-key --init
sudo pacman-key --add - <<< "${key}"
sudo pacman-key --lsign-key "${fingerprint}"
sudo pacman -Sy home_akselmo_kate-git-arch_Arch/kate-git

echo "Install chaotic aur"
sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key 3056513887B78AEB
sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
echo -e "\n" | sudo tee -a /etc/pacman.conf
echo "[chaotic-aur]" | sudo tee -a /etc/pacman.conf
echo "Include = /etc/pacman.d/chaotic-mirrorlist" | sudo tee -a /etc/pacman.conf

sudo pacman -S yay
#echo "Enable custom cursors for flatpak"
#flatpak --user override --filesystem=/home/$USER/.icons/:ro
#flatpak --user override --filesystem=/usr/share/icons/:ro

echo "Last bit needs manual work:"
echo "Setup nextcloud"
echo "Setup email"
echo "Setup calendar"
echo "Setup password manager"
echo "Add your gpg key with kleopatra"
echo "Set up all backups in their place"
echo "Get the server aliases"
echo "And finally, set up the custom fonts from nextcloud"
echo "Then, reboot! Check logseq for more."