import requests
import html
import sys


if len(sys.argv) <= 1:
    print("No args given!")
    exit(1)

url = sys.argv[1]
header = {
    "headers": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0"
}
request = requests.get(url, headers=header)
request_text = request.text
print(
    "["
    + html.unescape(
        request_text[request_text.find("<title>") + 7 : request_text.find("</title>")]
    )
    + "]("
    + url
    + ")",
    end=''
)
