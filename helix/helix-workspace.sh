#!/usr/bin/env bash

export HELIX_RUNTIME=/home/akseli/Repositories/helix/runtime

toplevel=$(git rev-parse --show-toplevel)

if [[ -z "$toplevel" ]]; then
   /home/akseli/Repositories/helix/target/release/hx $1 $2
else
    /home/akseli/Repositories/helix/target/release/hx -w $toplevel $1 $2
fi
